<?php

namespace D3x\Metakocka;

use d3x\starter\StarterServiceProvider;
use Illuminate\Support\ServiceProvider;

class MetakockaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/metakocka.php' => config_path('metakocka.php'),
        ]);
    }

    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/metakocka.php', 'metakocka'
        );
    }
}
