<?php


return [
    "connection" => [
        "url" => env("MK_URL", "https://main.metakocka.si"),
        "secret" => env("MK_SECRET_KEY", ""),
        "company_id" => env("MK_COMPANY_ID", ""),
    ],
];
