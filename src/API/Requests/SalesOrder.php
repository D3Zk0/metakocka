<?php

namespace D3x\Metakocka\API\Requests;

use D3x\Metakocka\API\MetakockaRequest;

class SalesOrder extends MetakockaRequest
{
    public function __construct()
    {
        $this->action = "/rest/eshop/v1/put_document";
        $this->postfields["doc_type"] = "sales_order";
        parent::__construct();
    }

    /**
     * Merges raw data into the existing postfields.
     *
     * Associative array containing specific details for the Sales Order with the following keys:
     *
     *   - 'count_code' (string): Order count code. Required.
     *   - 'doc_date' (string): Document date in ISO format, e.g., '2014-11-23+02:00'. Required.
     *   - 'warehouse_delivery' (string): The warehouse for delivery, e.g., 'glavni'. Required.
     *   - 'discount_value' (string): Discount value. Optional.
     *   - 'currency_code' (string): The currency code, e.g., 'USD'. Required.
     *   - 'title' (string): Title of the document, e.g., 'narocilnica 1'. Required.
     *   - 'delivery_date' (string): Expected delivery date in ISO format, e.g., '2014-11-22+02:00'. Required.
     *   - 'offer_number' (string): Offer number, e.g., 'ponudba1'. Optional.
     *   - 'purchase_pricelist_code' (string): The code of the purchase price list, e.g., 'cenik_pr_1'. Optional.
     *   - 'pariteta' (string): Delivery term, e.g., 'Lasten Prevzem'. Optional.
     *   - 'delivery_type' (string): Type of delivery, e.g., 'Logo'. Optional.
     *   - 'notes' (string): Short description or notes. Optional.
     *   - 'partner' => added with addPartner()
     *   - 'receiver' => added with addReceiver
     *   - 'product_list' => added with addProducts() || addProduct()
     *
     * If a key already exists in the postfields, the new value will be merged without overriding the existing value.
     * @param array $data
     * @return $this
     */

    public function setRaw(array $data)
    {
        if (is_array($this->postfields)) {
            $this->postfields = array_merge_recursive($this->postfields, $data);
        } else {
            $this->postfields = $data;
        }
        return $this;
    }

    /**
     * Sets the partner details for the sales order.
     *
     * Parameters:
     *   $partner (array) Associative array with keys:
     *     - 'business_entity' (bool)    : Indicates business entity status. Required.
     *     - 'taxpayer' (bool)           : Indicates taxpayer status. Required.
     *     - 'foreign_county' (bool)     : Indicates foreign country status. Required.
     *     - 'tax_id_number' (string)    : Tax ID. Required.
     *     - 'customer' (string)         : Customer name. Required.
     *     - 'street' (string)           : Street address. Required.
     *     - 'post_number' (string)      : Postal code. Required.
     *     - 'place' (string)            : City or locality. Required.
     *     - 'province' (string)         : Province. Optional.
     *     - 'country' (string)          : Country. Required.
     *
     * Returns:
     *   SalesOrder
     *
     * @param array $partner
     * @return $this
     */
    public function setPartner(array $partner)
    {
        $this->postfields['partner'] = $partner;
        return $this;
    }

    /**
     * Sets the receiver details for the sales order.
     * Associative array with keys:
     *   - 'business_entity'  (bool)   : Indicates business entity status. Required.
     *   - 'taxpayer'         (bool)   : Indicates taxpayer status. Required.
     *   - 'foreign_county'   (bool)   : Indicates foreign country status. Required.
     *   - 'tax_id_number'    (string) : Tax ID. Required.
     *   - 'customer'         (string) : Customer or receiver name. Required.
     *   - 'street'           (string) : Street address. Required.
     *   - 'post_number'      (string) : Postal code. Required.
     *   - 'place'            (string) : City or locality. Required.
     *   - 'country'          (string) : Country. Required.
     *   - 'partner_contact'  (array)  : Contact details (name, phone, etc.). Optional.
     * @param array $receiver
     * @return $this
     */
    public function setReceiver(array $receiver)
    {
        $this->postfields['receiver'] = $receiver;
        return $this;
    }

    /**
     *
     * Adds a product to the product list for the sales order.
     *
     *  Associative array containing the product details with the following keys:
     *   - 'code' (string): The code or identifier for the product. Required.
     *   - 'amount' (string): The quantity or amount for the product. Required.
     *   - 'price' (string): The price of the product. Required.
     *   - 'tax' (string): The tax identifier or code for the product. Required.
     * @param array $product
     * @return $this
     */
    public function addProduct(array $product)
    {
        $this->postfields['product_list'][] = $product;
        return $this;
    }

    /**
     * Adds multiple products to the product list for the sales order.
     *
     *  An array of associative arrays, each containing the product details with the following keys:
     *   - 'code' (string): The code or identifier for the product. Required.
     *   - 'amount' (string): The quantity or amount for the product. Required.
     *   - 'price' (string): The price of the product. Required.
     *   - 'tax' (string): The tax identifier or code for the product. Required. 000 - 0%;EXT3 - 9.5%; EXT4 - 22%;
     * @param $products
     * @return $this
     */
    public function setProducts($products)
    {
        foreach ($products as $product) {
            $this->addProduct($product);
        }
        return $this;
    }

}
