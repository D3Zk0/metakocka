<?php

namespace D3x\Metakocka\API\Requests;

use D3x\Metakocka\API\MetakockaRequest;

class GetPDF extends MetakockaRequest
{

    public function __construct($mk_id, $report_id, $params = [])
    {
        $this->action = "/rest/eshop/v1/report";
        $this->postfields["report_id"] = $report_id;
        $this->postfields["mk_id"] = $mk_id;
        if ($params)
            $this->postfields["params"] = $params;
        parent::__construct();
        $this->execute();
    }


}
