<?php

namespace D3x\Metakocka\API\Requests;

class Search extends \D3x\Metakocka\API\MetakockaRequest
{

    public function __construct($type)
    {
        $this->action = "/rest/eshop/v1/search";
        $this->postfields["doc_type"] = $type;
        parent::__construct();
    }


    /**
     * Sets the partner details for the sales order.
     *
     * Parameters:
     *   $partner (array) Associative array with keys:
     *     - 'doc_type' (string)            : Document type, e.g., "sales_order".
     *     - 'result_type' (string)         : The type of result expected, e.g., "doc".
     *     - 'limit' (int)                  : The maximum number of records to return.
     *     - 'offset' (int)                 : The starting point for record retrieval.
     *     - 'query_advance' (array)        : Array of query conditions. Each element is an associative array with 'type' and 'value'.
     *
     *
     * @param array $options
     * @return $this
     */

    public function setRaw(array $options)
    {
        if (is_array($this->postfields)) {
            $this->postfields = array_merge_recursive($this->postfields, $options);
        } else {
            $this->postfields = $options;
        }
        return $this;
    }

    public function search(){
        $this->execute();
    }

}
