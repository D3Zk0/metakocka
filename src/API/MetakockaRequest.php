<?php

namespace D3x\Metakocka\API;

use D3x\Metakocka\API\Exceptions\CurlException;
use D3x\Metakocka\API\Exceptions\MKException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class MetakockaRequest
{
    protected $url;
    protected $action;
    protected $method;
    protected $secret;
    protected $company_id;
    public $postfields = null;
    public $response;

    protected const HEADERS = [
        'Content-Type: application/json'
    ];

    public function __construct()
    {
        $this->url = Config::get('metakocka.connection.url');
        $this->secret = Config::get('metakocka.connection.secret');
        $this->company_id = Config::get('metakocka.connection.company_id');
        $this->method = "POST";
        $this->postfields["secret_key"] = $this->secret;
        $this->postfields["company_id"] = $this->company_id;
    }

    public function getOptions()
    {
        $options = [
            CURLOPT_URL => $this->url . $this->action,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_HTTPHEADER => self::HEADERS,
        ];

        if (in_array($this->method, ['GET', 'get']) && $this->postfields) {
            $query_string = http_build_query($this->postfields);
            if (!empty($query_string))
                $options[CURLOPT_URL] .= '?' . $query_string;
        }
        if ($this->postfields)
            $options[CURLOPT_POSTFIELDS] = json_encode($this->postfields);
        return $options;
    }

    public function execute()
    {
        $curl = curl_init();
        $options = $this->getOptions();
        curl_setopt_array($curl, $options);

        // Pridobi vsebino Content-Type iz informacij
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $contentType = $info['content_type'];

        if (curl_errno($curl)) {
            throw new CurlException($curl);
        } else {
            //$contentType === 'application/pdf'
            if ($contentType === 'application/pdf') {
                $this->response = (object)[];
                $this->response->pdf = base64_encode($response);
            } else {
                $this->response = json_decode($response);
                if ($this->response->opr_code != "0")
                    throw new MKException($options, $response);
            }
        }
        curl_close($curl);
    }

}
