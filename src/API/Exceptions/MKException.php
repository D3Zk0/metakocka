<?php

namespace D3x\Metakocka\API\Exceptions;

use Illuminate\Support\Facades\Log;

class MKException extends \Exception
{
    public function __construct($options, $response)
    {
        // Pridobite curl možnosti
        $options['response'] = $response;
        // Zapišite curl možnosti v log
        Log::error("Napaka pri metakocka zahtevku", $options);
//        dd($response, $options);
        // Pokličite konstruktor nadrazreda
        parent::__construct("Napaka pri Metakocka zahtevku.");
    }
}
